<?php

namespace hugenet\smsPackage;

use Illuminate\Support\ServiceProvider;

class SMSServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('sms', function () {
            return new Sms();
        });

        $this->mergeConfigFrom(__DIR__.'/Config/main.php','sms');
    }

    public function boot(){
        $this->publishes([
            __DIR__.'/Config/main.php' => config_path('sms.php')
        ]);
    }
}