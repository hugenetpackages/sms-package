<?php

namespace hugenet\smsPackage;

use GuzzleHttp\Client;

class Sms
{
    private $username;
    private $password;
    private $from;
    private $to;
    private $token;

    public function __construct()
    {
        $this->username = config('sms.username');
        $this->password = config('sms.password');
        $this->from = config('sms.from');
    }

    public function setReceiver(array $to)
    {
        $this->to = $to;
        return $this;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function sendSMS(string $message)
    {
        $url = "188.0.240.110/services.jspd";
        $param = array(
            'uname' => $this->username,
            'pass' => urlencode($this->password),
            'from' => $this->from,
            'message' => $message,
            'to' => json_encode($this->to),
            'op' => 'send');

        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handler, CURLOPT_POSTFIELDS, $param);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        curl_exec($handler);
    }

    public function sendPatternSMS($pattern, array $data)
    {
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 0);
        $variables = [];
        $values = [];
        foreach ($data as $key => $value) {
            $variables[] = $key;
            $values[] = $value;
        }
        $text = "";
        $i = 1;
        foreach ($data as $key => $variable) {
            $text .= "&p" . ($i++) . '=' . $key;
        }
        $i = 1;
        foreach ($data as $key => $value) {
            $text .= "&v" . ($i++) . '=' . (urlencode($value));
        }
        $url = ('http://ippanel.com:8080/?apikey=' . ($this->token) . '&pid=' . $pattern . '&fnum=' . ($this->from) . '&tnum=' . ($this->to[0]) . $text);

        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $response2 = curl_exec($handler);
        return json_decode($response2);
    }
}